﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace elevenZero.Exceptions
{
    /// <summary>
    /// 
    /// </summary>
    public enum UserErrorType
    {
        /// <summary>
        /// Unknown error
        /// </summary>
        UnknownError,
        /// <summary>
        /// Login failed
        /// </summary>
        LoginFailed,
        /// <summary>
        /// No user and userprofile associations in the db
        /// </summary>
        NoAssociations,
        /// <summary>
        /// Account is locked
        /// </summary>
        AccountLocked
    }

    /// <summary>
    /// Quick helper class for simple UserErrorExceptions.  Feel free to override and expand.
    /// </summary>
    public static class UserErrorException
    {
        /// <summary>
        /// Given a UserErrorType, returns an error message.
        /// </summary>
        /// <param name="errorType"></param>
        /// <returns></returns>
        public static Exception FromType(UserErrorType errorType)
        {
            string error;
            switch (errorType)
            {
                case UserErrorType.AccountLocked :
                    error = "Account locked";
                    break;
                case UserErrorType.LoginFailed:
                    error = "Login failed";
                    break;
                case UserErrorType.NoAssociations:
                    error = "No associations";
                    break;
                default:
                    error = "Unknown error.";
                    break;
            }
            return new Exception(error);
        }
    }
}
