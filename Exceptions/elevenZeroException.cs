﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace elevenZero.Exceptions
{
    /// <summary>
    /// 
    /// </summary>
    public class elevenZeroException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public elevenZeroExceptionType Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        /// <param name="type"></param>
        public elevenZeroException(string error, elevenZeroExceptionType type) : base(error)
        {
            Type = type;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        public elevenZeroException(string error = "An unknown error has occurred")
            : base(error)
        {
            Type = elevenZeroExceptionType.UKNOWN;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public elevenZeroException(elevenZeroExceptionType type = elevenZeroExceptionType.UKNOWN)
            : base("An unknown error has occurred.  Please refer to Exception.Type for more details.")
        {
            Type = type;
        }

    }

    /// <summary>
    /// Enum class for determining elevenZeroException type
    /// </summary>
    public enum elevenZeroExceptionType
    {
        /// <summary>
        /// An unknown error has occurred
        /// </summary>
        UKNOWN,
        /// <summary>
        /// Property is marked as read-only
        /// </summary>
        READONLY,
        /// <summary>
        /// Entity failed validation
        /// </summary>
        VALIDATION_FAILED
    }

    /// <summary>
    /// 
    /// </summary>
    public static class elevenZeroExceptionHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string MessageForType(elevenZeroExceptionType type)
        {
            return "";
        }

        /// <summary>
        /// todo: needs love
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static elevenZeroException Exception(elevenZeroExceptionType type)
        {
            return new elevenZeroException(type);
        }
    }
}
