﻿// ReSharper disable CSharpWarnings::CS0618
// ReSharper disable InconsistentNaming
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace elevenZero.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class EncryptionHelper
    {
        #region constant values

        private static readonly EncryptionSettings _settings = elevenZeroConfig.Settings.Encryption;

        /// <summary>
        /// Password embedded into the encryption.  Note, changing this value will cause decryptions with the previous value to fail!
        /// </summary>
        public static string EncryptionHelperPassword = _settings.Key;
        /// <summary>
        /// Salt to decrypt with.   Note, changing this value will cause decryptions with the previous value to fail!
        /// </summary>
        public static string EncryptionHelperSalt = _settings.Salt;
        /// <summary>
        /// Can be either SHA1 or MD5
        /// </summary>
        public static string EncryptionHelperHashAlgorithm = _settings.Algorithm;
        /// <summary>
        /// MUST be 16 ASCII characters.  Note, changing this value will cause decryptions with the previous value to fail!
        /// </summary>
        public static string EncryptionHelperInitialVector = _settings.Vector;
        /// <summary>
        /// Number of tries to attempt password.  Threading can sometimes cause the first pass to fail...
        /// </summary>
        public static int EncryptionHelperPasswordIterations = 2;
        /// <summary>
        /// Must be 128, 192 or 256.  The bit-level depth of the encryption.
        /// </summary>
        public static int EncryptionHelperKeySize = 256;
        #endregion

        /// <summary>
        /// Encrypts a string
        /// </summary>
        /// <returns>An encrypted string on pass, the original string on fail.</returns>
        public static string Encrypt(this String text)
        {
            if (string.IsNullOrEmpty(text)) return "";
            try
            {
                byte[] initialVectorBytes = Encoding.ASCII.GetBytes(EncryptionHelperInitialVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(EncryptionHelperSalt);
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(text);

                var derivedPassword = new PasswordDeriveBytes(EncryptionHelperPassword, saltValueBytes,
                    EncryptionHelperHashAlgorithm, EncryptionHelperPasswordIterations);
                var keyBytes = derivedPassword.GetBytes(EncryptionHelperKeySize/8);

                var symmetricKey = new RijndaelManaged {Mode = CipherMode.CBC};

                byte[] cipherTextBytes;

                using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, initialVectorBytes))
                {
                    using (var memStream = new MemoryStream())
                    {
                        using (var cryptoStream = new CryptoStream(memStream, encryptor, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                            cryptoStream.FlushFinalBlock();
                            cipherTextBytes = memStream.ToArray();
                            memStream.Close();
                            cryptoStream.Close();
                        }
                    }
                }
                symmetricKey.Clear();

                return Convert.ToBase64String(cipherTextBytes);
            }
            catch (Exception e)
            {
                elevenZeroConfig.Throw(e);
            }
            return text;
        }

        /// <summary>
        /// Decrypts a string
        /// </summary>
        /// <returns>A decrypted string on pass, the original string on fail.</returns>
        public static string Decrypt(this String text)
        {
            if (string.IsNullOrEmpty(text)) return "";
            try
            {
                var initialVectorBytes = Encoding.ASCII.GetBytes(EncryptionHelperInitialVector);
                var saltValueBytes = Encoding.ASCII.GetBytes(EncryptionHelperSalt);
                var cipherTextBytes = Convert.FromBase64String(text);

                var derivedPassword = new PasswordDeriveBytes(EncryptionHelperPassword, saltValueBytes,
                    EncryptionHelperHashAlgorithm, EncryptionHelperPasswordIterations);
                var keyBytes = derivedPassword.GetBytes(EncryptionHelperKeySize / 8);

                var symmetricKey = new RijndaelManaged {Mode = CipherMode.CBC};

                var plainTextBytes = new byte[cipherTextBytes.Length];
                int byteCount;

                using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, initialVectorBytes))
                {
                    using (var memStream = new MemoryStream(cipherTextBytes))
                    {
                        using (var cryptoStream = new CryptoStream(memStream, decryptor, CryptoStreamMode.Read))
                        {
                            byteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                            memStream.Close();
                            cryptoStream.Close();
                        }
                    }
                }
                symmetricKey.Clear();
                return Encoding.UTF8.GetString(plainTextBytes, 0, byteCount);
            }
            catch (Exception e)
            {
                elevenZeroConfig.Throw(e);
            }
            return text;
        }
    }
}
// ReSharper restore CSharpWarnings::CS0618
// ReSharper restore InconsistentNaming