﻿using System;
using System.ComponentModel.DataAnnotations;

namespace elevenZero.Models.Base
{
    /// <summary>
    /// The base entity class that all objects subscribing to the UnitOfWork must inherit from.
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// The Primary Key for the BaseEntity object.
        /// </summary>
        [Key]
        public Guid EntityID { get; set; }

        /// <summary>
        /// The base entity class that all objects subscribing to the UnitOfWork must inherit from.
        /// </summary>
        protected BaseEntity()
        {
            // BaseEntities must have a valid guid to start with.
            EntityID = Guid.NewGuid();
            CreatedOn = LastUpdated = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        protected DateTime CreatedOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LastUpdated { get; set; }
    }
}