﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using elevenZero.Exceptions;
using elevenZero.Models.Interfaces;
using elevenZero.Repository;
using WebMatrix.WebData;

namespace elevenZero.Models.Base
{
    /// <summary>
    /// 
    /// </summary>
    [Table("WebSecurityProfile")]
    public class WebSecurityUser
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Store a reference to the BaseEntity User
        /// </summary>
        public Guid UserGuid { get; set; }
    }

    public static class SecurityHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="autoCreateTables"></param>
        public static void InitializeDb(string connectionString, bool autoCreateTables = true)
        {
            WebSecurity.InitializeDatabaseConnection(connectionString, "WebSecurityProfile", "UserId", "UserName", autoCreateTables);
        }

        /// <summary>
        /// Return the BaseEntity&lt;T&gt; user by Guid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UoW"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static T UserById<T>(this UnitOfWork UoW, Guid userId) where T : BaseEntity
        {
            return UoW.Get<T>(userId);
        }

        /// <summary>
        /// Return the &lt;WebSecurityUser&gt; (IBaseUser) &lt;T&gt; using the WebSecurityUser int ID.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UoW"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static T UserProfileById<T>(this UnitOfWork UoW, int id) where T : IBaseUser, new()
        {
            var user = UoW.Context.Set<WebSecurityUser>().FirstOrDefault(p => p.UserId == id);
            if (user == null) return new T();

            return new T
            {
                WebProSecurityUser = user,
                User = UoW.UserById<BaseEntity>(user.UserGuid)
            };
        }

        /// <summary>
        /// Return the &lt;WebSecurityUser&gt; (IBaseUser) &lt;T&gt; using the WebSecurityUser email.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UoW"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public static T UserProfileByEmail<T>(this UnitOfWork UoW, string email) where T : IBaseUser, new()
        {
            var user = UoW.Context.Set<WebSecurityUser>().FirstOrDefault(p => p.Email == email);
            if (user == null) return new T();

            return new T
            {
                WebProSecurityUser = user,
                User = UoW.UserById<BaseEntity>(user.UserGuid)
            };
        }


        /// <summary>
        /// Return the &lt;WebSecurityUser&gt; (IBaseUser) &lt;T&gt; using the WebSecurityUser UserName.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UoW"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public static T UserProfileByUsername<T>(this UnitOfWork UoW, string username) where T : IBaseUser, new()
        {
            var user = UoW.Context.Set<WebSecurityUser>().FirstOrDefault(p => p.UserName == username);
            if (user == null) return new T();

            return new T
            {
                WebProSecurityUser = user,
                User = UoW.UserById<BaseEntity>(user.UserGuid)
            };
        }

        /// <summary>
        /// Log the user out using WebSecurity.Logout();
        /// </summary>
        public static void Logout()
        {
            WebSecurity.Logout();
        }

        /// <summary>
        /// Attempts to log the user in using the user's email and password.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UoW"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static T TryLogin<T>(this UnitOfWork UoW, ILoginModel model) where T: IBaseUser, new ()
        {
            if (!WebSecurity.Login(model.Email, model.Password, model.RememberMe))
            {
                if (WebSecurity.UserExists(model.Email) && (WebSecurity.IsAccountLockedOut(model.Email, -1, -1)))
                {
                    throw UserErrorException.FromType(UserErrorType.AccountLocked);
                }
                throw UserErrorException.FromType(UserErrorType.LoginFailed);
            }

            try
            {
                // override of CurrentUser get, since WebSecurity may not
                // recognize .IsAuthenticate until after page load.
                // Hey m$, ever heard of RESTful services?  sheesh
                var profileId = WebSecurity.GetUserId(model.Email);
                var profile = UoW.Context.Set<WebSecurityUser>().FirstOrDefault(u => u.UserId == profileId);
                if (profile == null) throw UserErrorException.FromType(UserErrorType.NoAssociations);
                return UoW.UserProfileById<T>(profile.UserId);
            }
            catch (Exception e)
            {
                Logout();
                throw new Exception("An error occurred.", e);
            }
        }

    }
}
