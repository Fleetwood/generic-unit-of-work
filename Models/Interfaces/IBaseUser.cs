﻿using elevenZero.Models.Base;

namespace elevenZero.Models.Interfaces
{
    /// <summary>
    /// Interface for associating a User class to SimpleMembership provider
    /// </summary>
    public interface IBaseUser
    {
        /// <summary>
        /// 
        /// </summary>
        BaseEntity User { get; set; }
        /// <summary>
        /// 
        /// </summary>
        WebSecurityUser WebProSecurityUser { get; set; }
    }
}
