﻿
namespace elevenZero.Models.Interfaces
{
    /// <summary>
    /// Interface for logging in to SimpleMembership security model
    /// </summary>
    public interface ILoginModel
    {
        /// <summary>
        /// User's email address
        /// </summary>
        string Email { get; set; }
        /// <summary>
        /// User's password
        /// </summary>
        string Password { get; set; }
        /// <summary>
        /// Whether or not to store a cookie for login
        /// </summary>
        bool RememberMe { get; set; }
    }
}
