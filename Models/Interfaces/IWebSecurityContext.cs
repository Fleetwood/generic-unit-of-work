﻿using System.Configuration;
using elevenZero.Models.Base;
using System.Data.Entity;

namespace elevenZero.Models.Interfaces
{
    /// <summary>
    /// A DbContext that implements a WebSecurity user DbSet property.
    /// </summary>
    public interface IWebSecurityContext
    {
        /// <summary>
        /// Get WebSecurity users.  Note that this class does NOT subscribe to 
        /// BaseEntity class restrictions, and cannot be access via the Generic Unit of Work. 
        /// SEE <see cref="BaseEntity"/>
        /// SEE ALSO <seealso cref="WebSecurityUser"/>
        /// </summary>
        DbSet<WebSecurityUser> WebSecurityUsers { get; set; }
    }

    /// <summary>
    /// A DbContext that implements SimpleMembership rows in the db.
    /// </summary>
    public class WebSecurityContext : DbContext, IWebSecurityContext
    {
        /// <summary>
        /// Whether or not to automatically create the SimpleMembership tables OnModelCreating.
        /// </summary>
        public bool AutoCreateTables { get; set; }

        /// <summary>
        /// The nanme of the connection string passed to the constructor.  Default: "DefaultConnection".
        /// </summary>
        public string ConnectionStringName { get; set; }

        /// <summary>
        /// Standard DbContext constructor.  
        /// </summary>
        /// <param name="connectionString">Default: "DefaultConnection"</param>
        /// <param name="autoCreateTables">Whether or not to automatically create the SimpleMembership tables OnModelCreating.</param>
        protected WebSecurityContext(string connectionString = "DefaultConnection", bool autoCreateTables = true)
            : base(connectionString)
        {
            ConnectionStringName = connectionString;
            AutoCreateTables = autoCreateTables;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            SecurityHelper.InitializeDb(ConnectionStringName, AutoCreateTables);
        }

        public DbSet<WebSecurityUser> WebSecurityUsers { get; set; }
    }
}
