﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("GenericUnitOfWork")]
[assembly: AssemblyDescription("A Generic EF 6.0 UnitOfWork and Repository class.  Give it a context, and all of the CRUD methods are in place for you.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("elevenZero Media LLC")]
[assembly: AssemblyProduct("GenericUnitOfWork")]
[assembly: AssemblyCopyright("Copyright ©2014, elevenZero Media, LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("129e167c-82dc-4b42-96bc-a5505e12702a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.0.0")]
[assembly: AssemblyFileVersion("1.2.0.0")]
