﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using elevenZero.Models.Base;

namespace elevenZero.Repository.Interfaces
{
    /// <summary>
    /// Interface for the Repository class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// The DbContext object for the Entity type.
        /// </summary>
        DbContext Context { get; }

        /// <summary>
        /// The DbContext Set for the Entity type.
        /// </summary>
        DbSet<T> Set { get; }

        /// <summary>
        /// Returns true if the entity object exists in the Context.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Exists(T entity);

        /// <summary>
        /// Returns true if the &lt;T&gt; object exists in the DbSet.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Exists(Guid id);

        /// <summary>
        /// Returns true if any single &lt;T&gt; object matches the Lambda predicate, false if no matches.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        bool Any(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Get the entity object that satisfies the Lambda predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        T Get(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Get the entity object that matches the provided id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Get(Guid id);

        /// <summary>
        /// Gets all object in the <code>DbSet</code> that satisfy the Lambda predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IQueryable<T> All(Expression<Func<T, bool>> predicate = null);

        /// <summary>
        /// Obtain an Entity object and perform Eager Load of all associated POCOs.  Note that this does not query and map the corollary objects,
        /// it only populates the included properties.  Multiple properties can be included using comma-separated Expressions, e.g. 
        /// GetIncluding(i => i.UserProperties, j => j.UserInformations) etc.
        /// </summary>
        /// <param name="includeExpressions"></param>
        /// <returns></returns>
        IQueryable<T> GetIncluding(params Expression<Func<T, object>>[] includeExpressions);

        /// <summary>
        /// Obtain an Entity object and perform Eager Load of all associated POCOs.  Note that this does not map the corollary objects,
        /// it only populates the included Properties.  Multiple properties can be included using comma-separated strings, e.g. 
        /// GetIncluding("UserAttributes","UserAddresses")
        /// </summary>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        IQueryable<T> GetIncluding(string includeProperties);

        /// <summary>
        /// Used to pass a Stored Procedure call to the Context.
        /// </summary>
        /// <remarks>Type &lt;T&gt; The Entity type.  Note: Should be mapped to the StoredProcedure as a complex type, not a POCO object.</remarks>
        /// <param name="name">Name of the Stored Procedure in the database.</param>
        /// <param name="parameters">Stored Procedure paramters.</param>
        /// <returns>Complex type mapped to the specified Stored Procedure.</returns>
        IQueryable<T> StoredProcedure(string name, object parameters = null);

        /// <summary>
        /// Gets the number of rows in the entity Context.
        /// </summary>
        int Total { get; }

        /// <summary>
        /// Update and insert.  If the object exists, it will be updated.  If not, it will be inserted.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Upsert(T entity);
        
        /// <summary>
        /// Delete the supplied entity from the database.  Returns true if successful, false on fail.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Delete(T entity);

        /// <summary>
        /// Delete the entity matching the supplied Guid id from the database.  Returns true if successful, false on fail.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(Guid id);

    }
}
