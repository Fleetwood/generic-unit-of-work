﻿using System;
using System.Linq;
using System.Linq.Expressions;
using elevenZero.Models.Base;

namespace elevenZero.Repository.Interfaces
{
    /// <summary>
    /// Thread-safe Singleton Repository wrapper BaseEntity that also manages Entity-specific Repositories.  Repositories
    /// need not be instantiated, the UnitOfWork will handle that for you.
    /// </summary>
    /// For further reading on UnitOfWork pattern:
    /// http://elegantcode.com/2009/12/15/entity-framework-ef4-generic-repository-and-unit-of-work-prototype/
    /// <see cref="Repository"/>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Get a BaseEntity&lt;T&gt; by Guid id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        T Get<T>(Guid id) where T : BaseEntity;
        
        /// <summary>
        /// Get a BaseEntity&lt;T&gt; by Guid id, with lambda expression filter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="predicate"></param>
        /// <returns></returns>
        T Get<T>(Expression<Func<T, bool>> predicate = null) where T : BaseEntity;

        /// <summary>
        /// Will lazy load associated objects whose id matches the supplied Guid, and whose property names match the supplied string.  
        /// Multiple properties may be included by separating with commas.
        /// </summary>
        /// <example>GetIncluding(id, "AssociatedValues,AssociatedTags") will also load AssociatedValues objects and AssociatedTags by PK -&gt; FK.</example>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        T GetIncluding<T>(Guid id, string includeProperties) where T : BaseEntity;

        /// <summary>
        /// Will lazy load associated objects whose property names match the supplied string.  Multiple properties may be included by separating with commas.
        /// </summary>
        /// <example>GetIncluding(id, "AssociatedValues,AssociatedTags") will also load AssociatedValues objects and AssociatedTags by PK -&gt; FK.</example>
        /// <typeparam name="T"></typeparam>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        IQueryable<T> GetIncluding<T>(string includeProperties) where T : BaseEntity;

        /// <summary>
        /// Get All objects of this type from the database, filtering by lambda expression.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IQueryable<T> All<T>(Expression<Func<T, bool>> predicate = null) where T : BaseEntity;

        /// <summary>
        /// Call a StoredProcedure on the UnitOfWork context, with parameters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        IQueryable<T> StoredProcedure<T>(string name, object parameters = null) where T : BaseEntity;

        /// <summary>
        /// Get the total count of all objects of type &lt;T&gt;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        int Total<T>() where T : BaseEntity;

        /// <summary>
        /// Determine if the supplied entity exists in the database.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Exists<T>(T entity) where T : BaseEntity;

        /// <summary>
        /// Returns true if any &lt;T&gt; object id matches.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Exists<T>(Guid id) where T : BaseEntity;

        /// <summary>
        /// Returns true if any single &lt;T&gt; object matches the Lambda predicate, false if no matches.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        bool Any<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity;

        /// <summary>
        /// If the supplied entity exists, Update it.  If not, Insert it.  Ergo, "Upsert".
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Upsert<T>(T entity) where T : BaseEntity;

        /// <summary>
        /// Delete the entity matching the supplied Guid id from the database.  Returns true if successful, false on fail.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        bool DeleteEntity<T>(Guid id) where T : BaseEntity;

        /// <summary>
        /// Delete the supplied entity from the database.  Returns true if successful, false on fail.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool DeleteEntity<T>(T entity) where T : BaseEntity;

        /// <summary>
        /// Commit all changes to the database.
        /// </summary>
        /// <returns></returns>
        bool Commit();
    }
}
