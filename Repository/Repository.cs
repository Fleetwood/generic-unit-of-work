﻿using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Text;
using elevenZero.Exceptions;
using elevenZero.Models.Base;
using elevenZero.Repository.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace elevenZero.Repository
{
    /// <summary>
    /// A generic Repository class for managing &lt;T&gt; DbSets. <code>DbContext</code> parameter is supplied by the <code>UnitOfWork</code> wrapper.
    /// </summary>
    public class Repository<T> : IRepository<T> where T: BaseEntity
    {
        /// <summary>
        /// A generic Repository class for managing &lt;T&gt; DbSets. <code>DbContext</code> parameter is supplied by the <code>UnitOfWork</code> wrapper.
        /// </summary>
        /// <param name="context">The DbContext to manage data connection.</param>
        public Repository(DbContext context)
        {
            _context = context;
        }

        private readonly DbContext _context;
        /// <summary>
        /// READ-ONLY. The database context.  This is populated upon instantiation within the constructor.
        /// </summary>
        public DbContext Context
        {
            get { return _context; }
        }

        /// <summary>
        /// READ-ONLY. The <code>DbSet</code> for the Generic Type &lt;T&gt;
        /// </summary>
        public DbSet<T> Set
        {
            get
            {
                try
                {
                    return Context.Set<T>();
                }
                catch (Exception e)
                {
                    Throw(e);
                }
                return null;
            }
        }

        private static void Throw(Exception e)
        {
            elevenZeroConfig.Throw(e);
        }

        /// <summary>
        /// Returns true if the &lt;T&gt; object exists in the DbSet.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Exists(T entity)
        {
            try
            {
                return Exists(entity.EntityID);
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return false;
        }

        /// <summary>
        /// Returns true if the &lt;T&gt; object exists in the DbSet.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Exists(Guid id)
        {
            try
            {
                return Set.Select(i => i.EntityID).Contains(id);
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return false;
        }

        /// <summary>
        /// Returns true if any single &lt;T&gt; object matches the Lambda predicate, false if no matches.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public bool Any(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return Set.Any(predicate);
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return false;
        }

        /// <summary>
        /// Gets all &lt;T&gt; objects in the <code>DbSet</code> that satisfy the optional Lambda predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns><code>null</code> if empty.</returns>
        /// <example>&lt;T&gt;All(i => i.id==param)</example>
        public IQueryable<T> All(Expression<Func<T, bool>> predicate = null)
        {
            try
            {
                return predicate != null ? Set.Where(predicate) : Set;
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return null;
        }

        /// <summary>
        /// Returns the total count of &lt;T&gt; objects in the <code>DbSet</code>
        /// </summary>
        public int Total
        {
            get { return Set.Count(); }
        }

        /// <summary>
        /// Update and insert.  If the &lt;T&gt; object exists, it will be updated.  If not, it will be inserted.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Upsert(T entity)
        {
            entity.LastUpdated = DateTime.Now;

            if (!Exists(entity)) return Add(entity);
            var e = Get(entity.EntityID);
            Context.Entry(e).CurrentValues.SetValues(entity);
            return Save();
        }

        /// <summary>
        /// Delete the &lt;T&gt; object from the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Delete(T entity)
        {
            if (!Exists(entity)) return false;
            try
            {
                if (Context.Entry(entity).State == EntityState.Detached) Set.Attach(entity);
                Set.Remove(entity);
                return true;
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return false;
        }

        /// <summary>
        /// Delete a &lt;T&gt;  object by the provided Guid id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            return Delete(Get(id));
        }

        /// <summary>
        /// Get the &lt;T&gt; entity object that matches the provided id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T Get(Guid id)
        {
            try
            {
                return Set.Find(id);
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return null;
        }

        /// <summary>
        /// Get the entity object that satisfies the Lambda predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        /// <example>&lt;T&gt;Get(i => i.id == param)</example>
        public T Get(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return Set.Where(predicate).FirstOrDefault();
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return null;
        }

        /// <summary>
        /// Used to pass a Stored Procedure call to the Context.
        /// </summary>
        /// <param name="name">Name of the Stored Procedure in the database.</param>
        /// <param name="parameters">Stored Procedure parameters.</param>
        /// <returns>Complex type mapped to the specified Stored Procedure.</returns>
        public IQueryable<T> StoredProcedure(string name, object parameters = null)
        {
            try
            {
                return Context.Database.SqlQuery<T>(name, parameters).AsQueryable();
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return null;
        }

        /// <summary>
        /// Obtain an Entity object and perform Eager Load of all associated POCOs.  Note that this does not map the corollary objects,
        /// it only populates the included Properties.  Multiple properties can be included using comma-separated Expressions, e.g. 
        /// GetIncluding(i => i.UserProperties, j => j.UserInformations) etc.
        /// </summary>
        /// <param name="includeExpressions"></param>
        /// <returns></returns>
        public IQueryable<T> GetIncluding(params Expression<Func<T, object>>[] includeExpressions)
        {
            return includeExpressions.Aggregate(Set.AsQueryable(), (u, e) => u.Include(e));
        }

        /// <summary>
        /// Obtain an Entity object and perform Eager Load of all associated POCOs.  Note that this does not map the corollary objects,
        /// it only populates the included Properties.  Multiple properties can be included using comma-separated strings, e.g. 
        /// GetIncluding("UserAttributes","UserAddresses")
        /// </summary>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        public IQueryable<T> GetIncluding(string includeProperties)
        {
            return includeProperties.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                                    .Aggregate<string, IQueryable<T>>(
                                        Set, 
                                        (current, includeProperty) => current.Include(includeProperty));
        }

        #region private methods
        private bool Save()
        {
            try
            {
                Context.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException ex)
            {
                if (elevenZeroConfig.Settings.ThrowErrors)
                {
                    //todo: provide a more detailed value here
                    elevenZeroExceptionHelper.Exception(elevenZeroExceptionType.VALIDATION_FAILED);
                }
                if (elevenZeroConfig.Settings.DebugMode)
                {
                    var sb = new StringBuilder();

                    foreach (var failure in ex.EntityValidationErrors)
                    {
                        sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                        foreach (var error in failure.ValidationErrors)
                        {
                            sb.AppendFormat("\t\tERROR {0} : {1}", error.PropertyName, error.ErrorMessage);
                            sb.AppendLine();
                        }
                    }
                    Debug.WriteLine(
                        "=========================\tEntity Validation Failed:\n{0}\n{1}\n=========================", 
                        sb,
                        ex);
                }
                return false;
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return false;
        }

        private bool Add(T entity)
        {
            if (Exists(entity)) return false;

            // all entities must have valid Guid ID
            if (IsBlankGuid(entity.EntityID)) entity.EntityID = NewGuid;

            try
            {
                Set.Add(entity);
                return true;
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return false;
        }

        /// <summary>
        /// Determines if the provided Guid is not blank.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public bool IsBlankGuid(Guid guid)
        {
            return guid.Equals(EmptyGuid);
        }

        private static Guid EmptyGuid
        {
            get
            {
                return Guid.Parse("00000000-0000-0000-0000-000000000000");
            }
        }

        private static Guid NewGuid
        {
            get { return Guid.NewGuid(); }
        }
        #endregion
    }
}