﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using elevenZero.Models.Base;
using elevenZero.Repository.Interfaces;

namespace elevenZero.Repository
{
    /// <summary>
    /// Thread-safe Singleton Repository wrapper BaseEntity that also manages Entity-specific Repositories.
    /// DbContext must be passed to the UnitOfWork after instantiation.  But individual repositories need 
    /// not be instantiated, the UnitOfWork will handle that for you.
    /// </summary>
    /// For further reading on UnitOfWork pattern:
    /// http://elegantcode.com/2009/12/15/entity-framework-ef4-generic-repository-and-unit-of-work-prototype/
    /// <see cref="elevenZero.Repository"/>
    public class UnitOfWork : IUnitOfWork
    {
        #region ctor
        private static UnitOfWork _instance;
        private static readonly object Padlock = new object();

        /// <summary>
        /// Ensures that the instance of UnitOfWork is a singleton object.
        /// </summary>
        public static UnitOfWork Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new UnitOfWork());
                }
            }
        }
        #endregion

        #region context and repos
        /// <summary>
        /// Instantiate a new Entities-based context.
        /// </summary>
        public DbContext Context { get; set; }

        /// <summary>
        /// Holder for Entity-based Repositories.
        /// </summary>
        private readonly List<Object> _repoList = new List<Object>();

        /// <summary>
        /// Getter helper method for the <code>Repo&lt;T&gt;</code> object of &lt;T&gt; type.  Method will
        /// attempt to return the requested <code>Repo&lt;T&gt;</code>, and if it isn't found, will 
        /// instantiate it and return it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <see>
        ///     AddRepo&amp;lt;T&amp;gt;
        /// </see>
        private Repository<T> Repo<T>() where T : BaseEntity
        {
            try
            {
                return
                    _repoList.FirstOrDefault(d => d.GetType() == typeof(Repository<T>)) as Repository<T>
                    ?? AddRepo<T>();
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return null;
        }

        private static void Throw(Exception e)
        {
            elevenZeroConfig.Throw(e);
        }

        /// <summary>
        /// Helper method for the <code>Repo&lt;T&gt;</code> getter.  Instantiates
        /// a new <code>Repository</code> of type&lt;T&gt;, adds it to the List,
        /// and returns it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private Repository<T> AddRepo<T>() where T : BaseEntity
        {
            var repo = new Repository<T>(Context);
            _repoList.Add(repo);
            return repo;
        }
        #endregion

        #region gc
        private bool _disposed;
        private void Dispose(bool disposing)
        {
            if (!_disposed && disposing && Context != null)
            {
                Context.Dispose();
            }
            _disposed = true;
        }

        /// <summary>
        /// Dispose of the UnitOfWork object.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region IUnitOfWork methods
        /// <summary>
        /// Obtain an object of type&lt;T&gt; by Guid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T Get<T>(Guid id) where T : BaseEntity
        {
            return Repo<T>().Get(id);
        }

        /// <summary>
        /// Obtain an object of type&lt;T&gt; by predicate.  Example: Get&lt;EntityObjecType&lt;(i => i.UserName == "John");
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public T Get<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity
        {
            var r = Repo<T>();
            return r.Get(predicate);
        }

        /// <summary>
        /// Obtain a collection of type&lt;T&gt;, and perform an Eager Load of associated POCO
        /// objects for that entity.  Note that this cannot resolve the related objects if the
        /// relational mapping was not created and managed by EF; it will only return the rows
        /// for those properties.  Since this returns an IQueryable&lt;T&gt;, it can be chained
        /// with additional LINQ methods, e.g. GetIncluding(...).Where(...).OrderBy(...) et al.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        /// <example>GetIncluding&lt;EntityType&lt;(i => i.EntityID)</example>
        public T GetIncluding<T>(Guid id, string includeProperties) where T : BaseEntity
        {
            var list = GetIncluding<T>(includeProperties);
            return list.FirstOrDefault(l => l.EntityID == id);
        }

        /// <summary>
        /// Obtain a collection of type&lt;T&gt;, and perform an Eager Load of associated POCO
        /// objects for that entity.  Note that this cannot resolve the related objects if the
        /// relational mapping was not created and managed by EF; it will only return the rows
        /// for those properties.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="includeProperties">Multiple properties can be included using comma-separated values, e.g. GetIncluding&lt;T&gt;("PropertyOne, PropertyTwo") etc.</param>
        /// <returns></returns>
        /// <example>GetIncluding&lt;T&lt;("PropertyItems")</example>
        public IQueryable<T> GetIncluding<T>(string includeProperties) where T : BaseEntity
        {
            var r = Repo<T>();
            return r.GetIncluding(includeProperties);
        }

        /// <summary>
        /// Returns the entire entity set that matches the predicate filter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="predicate">e.g. All&lt;T&gt;(i => i.Enabled == true)</param>
        /// <returns></returns>
        public IQueryable<T> All<T>(Expression<Func<T, bool>> predicate = null) where T : BaseEntity
        {
            return Repo<T>().All(predicate);
        }

        //public IEnumerable<T> Take<T, K>(int Count, Expression<Func<T, K>> orderBy, int Start = 0, Expression<Func<T, bool>> predicate = null) where T : BaseEntity
        //{
        //    return Repo<T>().Take(Count, orderBy, Start, predicate);
        //}

        /// <summary>
        /// Returns a collection of &lt;T&gt; objects by calling the StoredProcedure by name.  Note that
        /// object &lt;T&gt; must be a complex type mapped to an entity Function, not a standard POCO.
        /// </summary>
        /// <typeparam name="T">Complex entity types only.</typeparam>
        /// <param name="name">Name of the stored procedure in the database.</param>
        /// <param name="parameters">Complex parameter that can accept a single Object comprised of one or more ObjectParameters</param>
        /// <returns></returns>
        public IQueryable<T> StoredProcedure<T>(string name, object parameters = null) where T : BaseEntity
        {
            return Repo<T>().StoredProcedure(name, parameters);
        }

        /// <summary>
        /// Returns the total count of entity objects in the result set.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public int Total<T>() where T : BaseEntity
        {
            var r = Repo<T>();
            return r.Total;
        }

        /// <summary>
        /// Determines whether or not the provided &lt;T&gt; entity object exists in the context.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Exists<T>(T entity) where T : BaseEntity
        {
            return Exists<T>(entity.EntityID);
        }

        /// <summary>
        /// Returns true if any &lt;T&gt; object id matches.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Exists<T>(Guid id) where T : BaseEntity
        {
            var r = Repo<T>();
            return r.Exists(id);
        }

        /// <summary>
        /// Returns true if any single &lt;T&gt; object matches the Lambda predicate, false if no matches.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public bool Any<T>(Expression<Func<T, bool>> predicate) where T:BaseEntity
        {
            var r = Repo<T>();
            return r.Any(predicate);
        }

        /// <summary>
        /// Update and insert.  If the object exists, it will be updated.  If not, it will be inserted.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Upsert<T>(T entity) where T : BaseEntity
        {
            var r = Repo<T>();
            return r.Upsert(entity);
        }

        /// <summary>
        /// Delete the provided entity from the db that matches the provided Guid id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteEntity<T>(Guid id) where T : BaseEntity
        {
            var r = Repo<T>();
            return r.Delete(id);
        }

        /// <summary>
        /// Delete the provided entity from the db.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool DeleteEntity<T>(T entity) where T : BaseEntity
        {
            return DeleteEntity<T>(entity.EntityID);
        }

        /// <summary>
        /// Save all changes to the context.
        /// </summary>
        /// <returns></returns>
        public bool Commit()
        {
            try
            {
                Context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Throw(e);
            }
            return false;
        }
        #endregion

    }

    /// <summary>
    /// UnitOfWork Extension class
    /// </summary>
    /// <see cref="UnitOfWork"/>
    public static class UnitOfWorkHelper
    {
        /// <summary>
        /// Save all changes to the context.
        /// </summary>
        /// <see cref="UnitOfWork.Commit"/>
        /// <param name="uow"></param>
        /// <returns></returns>
        public static bool Commit(this UnitOfWork uow)
        {
            return uow.Commit();
        }
    }
}