﻿using System.Diagnostics;
using System.Runtime.CompilerServices;
// ReSharper disable InconsistentNaming
using System;
using System.Configuration;

namespace elevenZero
{
    /// <summary>
    /// Section to configure elevenZero dlls
    /// </summary>
    /// <remarks>
    /// Configuration must reside in &lt;elevenZero&gt; base section (config node)
    /// </remarks>
    /// <example>
    /// &lt;elevenZero debugMode="false" throwErrors="true" /&gt;
    /// </example>
    public class elevenZeroConfig : ConfigurationSection
    {
        private static readonly elevenZeroConfig _Settings = ConfigurationManager.GetSection("elevenZero") as elevenZeroConfig;

        /// <summary>
        /// Access global (UnitOfWork) and encryption settings.
        /// </summary>
        public static elevenZeroConfig Settings
        {
            get
            {
                return _Settings;
            }
        }

        /// <summary>
        /// Set to true to enable debug errors to be written to the Debug Output window.
        /// </summary>
        [ConfigurationProperty("debugMode",
            DefaultValue = false,
            IsRequired = false)]
        public bool DebugMode
        {
            get { return (bool)this["debugMode"]; }
            set { this["debugMode"] = value; }
        }

        /// <summary>
        /// Set to true to force exceptions in the GenericUnitOfWork to throw instead of ignoring.
        /// </summary>
        [ConfigurationProperty("throwErrors",
            DefaultValue = false,
            IsRequired = false)]
        public bool ThrowErrors
        {
            get { return (bool)this["throwErrors"]; }
            set { this["throwErrors"] = value; }
        }

        /// <summary>
        /// The EncryptionSettings for the encryption helper. 
        /// </summary>
        /// <see cref="EncryptionSettings"/>
        /// <remarks>Must reside in a child section of &lt;elevenZero /&gt;</remarks>
        [ConfigurationProperty("encryptionSettings",
            IsRequired = false)]
        public EncryptionSettings Encryption
        {
            get { return (EncryptionSettings) this["encryptionSettings"]; }
        }

        /// <summary>
        /// Use to throw an exception, based on Settings.ThrowErrors value.
        /// </summary>
        /// <param name="e"></param>
        /// <see cref="elevenZeroConfig.ThrowErrors"/>
        public static void Throw(Exception e)
        {
            if (Settings.ThrowErrors) throw e;
            if (Settings.DebugMode)
            {
                Console.Error.WriteLine("elevenZero Exception : " + e.Message);
                Debug.WriteLine("elevenZero Exception" + e.Message);
            }
            Debugger.Log(0, "elevenZero Exception", e.Message);
        }
    }

    /// <summary>
    /// Used to configure the encryption helper.
    /// </summary>
    public class EncryptionSettings : ConfigurationElement
    {
        /// <summary>
        /// Salt to decrypt with.
        /// </summary>
        /// <example>&lt;encryptionSettings salt="abcdefg" /></example>
        /// <remarks>Note, changing this value will cause decryptions against any previous values to fail!</remarks>
        [ConfigurationProperty("salt",
            IsRequired = true)]
        public string Salt
        {
            get { return (string)this["salt"]; }
            set { this["salt"] = value; }
        }

        /// <summary>
        /// Key embedded into the encryption.
        /// </summary>
        /// <example>&lt;encryptionSettings key="abcdefg" /></example>
        /// <remarks>Note, changing this value will cause decryptions against any previous values to fail!</remarks>
        [ConfigurationProperty("key",
            IsRequired = true)]
        public string Key
        {
            get { return (string)this["key"]; }
            set { this["key"] = value; }
        }
        
        /// <summary>
        /// MUST be 16 ASCII characters; recommended to leave this as default.  Best practice is to modify salt and key instead.
        /// </summary>
        /// <example>&lt;encryptionSettings initialVector="1234567890abcdef" /></example>
        /// <remarks>Note, changing this value will cause decryptions against any previous values to fail!</remarks>
        /// <see cref="Salt"/>
        /// <seealso cref="Key"/>
        [ConfigurationProperty("initialVector",
            DefaultValue = "OFRna73m*aze01xY",
            IsRequired = false)]
        public string Vector
        {
            get { return (string)this["initialVector"]; }
        }

        /// <summary>
        /// Compatible with SHA1 or MD5 only.  Default is SHA1.
        /// </summary>
        /// <example>&lt;encryptionSettings algorith="SHA1" /></example>
        /// <remarks>Note, changing this value will cause decryptions against any previous values to fail!</remarks>
        [ConfigurationProperty("algorithm",
            DefaultValue = "SHA1",
            IsRequired = false)]
        public string Algorithm
        {
            get { return (string)this["algorithm"]; }
        }
        
    }
}
// ReSharper restore InconsistentNaming